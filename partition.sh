#!/usr/bin/env bash

# define a disk and set partition sizes
disk='nvme0n1'
init_mibs='1'
boot_mibs='512'
swap_mibs=$(free -m | awk '/^Mem:/{print $2}')

disk_dev="/dev/${disk}"

# create a new disk label
parted -s "${disk_dev}" mklabel gpt

# calculate partition sizes
boot_from="${init_mibs}"
swap_from="$(expr ${boot_from} + ${boot_mibs})"
root_from="$(expr ${swap_from} + ${swap_mibs})"
# parted end position's are inclusive
boot_to="$(expr ${swap_from} - 1)"
swap_to="$(expr ${root_from} - 1)"

# create and format the boot partition
parted --script "${disk_dev}" mkpart boot fat32 "${boot_from}MiB" "${boot_to}MiB"
parted --script "${disk_dev}" set 1 esp on
boot_part="${disk}p1"
mkfs.fat -F 32 -n boot "/dev/${boot_part}"

# create and format swap partition
parted --script "${disk_dev}" mkpart swap linux-swap "${swap_from}MiB" "${swap_to}MiB"
swap_part="${disk}p2"
mkswap -L swap "/dev/${swap_part}"

# create and format root partition
parted --script "${disk_dev}" mkpart root "${root_from}MiB" 100%
root_part="${disk}p3"
mkfs.ext4 -L nixos "/dev/${root_part}"

# mounting
mount /dev/disk/by-label/nixos /mnt

mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

swapon "/dev/${swap_part}"

